%option noyywrap nodefault yylineno
%{
# include "thedbGrammar.h"
# include "thedbGrammar.tab.h"
%}

/* float exponent */
EXP	([Ee][-+]?[0-9]+)

%%
"+" |
"-" |
"*" |
"/" |
"|" |
"(" |
")" { return yytext[0]; }
[0-9]+"."[0-9]*{EXP}? |
"."?[0-9]+{EXP}? { yylval.d = atof(yytext); return NUMBER; }

[a-zA-Z][a-zA-Z0-9]* { printf("yytext=\"%s\"\n", yytext); yylval.s = yytext; return KEY; }

"str_set" { return STR_SET; }
"str_get" { return STR_GET; }

\n      { return EOL; }
"//".*  
[ \t]   { /* ignore white space */ }
.	{ yyerror("Mystery character %c\n", *yytext); }
%%

%{
#  include <stdio.h>
#  include <stdlib.h>
#  include "thedbGrammar.h"
%}

%union {
  struct ast *a;
  double d;
  char *s;
}

/* declare tokens */
%token <d> NUMBER
%token <s> STR_SET
%token <s> STR_GET
%token <s> KEY
%token EOL

// %type <a> STR_SET KEY

/* %type <a> exp factor term */

%%

thedb:
| thedb exp EOL {
  printf("> ");
}

exp:
  STR_SET KEY NUMBER { printf("str_set, %s, %f\n", $2, $3); }
  ;

/*
calclist:
| calclist exp EOL {
     printf("$2=%s\n", $2);
     printf("= %4.4g\n", eval($2));
     treefree($2);
     printf("> ");
 }

 | calclist EOL { printf(">> "); }
 ;

exp: factor
 | exp '+' factor { $$ = newast('+', $1,$3); }
 | exp '-' factor { $$ = newast('-', $1,$3);}
 ;

factor: term
 | factor '*' term { $$ = newast('*', $1,$3); }
 | factor '/' term { $$ = newast('/', $1,$3); }
 ;

term: NUMBER   { $$ = newnum($1); }
 | '|' term    { $$ = newast('|', $2, NULL); }
 | '(' exp ')' { $$ = $2; }
 | '-' term    { $$ = newast('M', $2, NULL); }
 ;

*/

%%